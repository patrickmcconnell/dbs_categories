//
//  NSDictionary+EmptyKeyMethods.m
//  QuadranWorkOrders
//
//  Created by Mark Menard on 5/1/12.
//  Copyright (c) 2012 Enable Labs. All rights reserved.
//

#import "NSDictionary+EmptyKeyMethods.h"

@implementation NSDictionary (EmptyKeyMethods)
- (NSString *)valueOrEmptyString:(NSString *)key
{
    NSString *result = [self valueForKey:key];
    if (!result || [result isKindOfClass:[NSNull class]]) {
        result = @"";
    }
    
    return [NSString stringWithFormat:@"%@", result];
}


- (NSData *)asJSON
{
    NSError *error = nil;
    return [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
}

- (NSDictionary *)stripNulls
{  
  NSMutableDictionary *dict = [self mutableCopy];
  [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop){
    if ([obj isKindOfClass:[NSNull class]] ) {
      dict[key]= @"";
    }
  }];
  return dict;
}

@end
