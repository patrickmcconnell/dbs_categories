//
//  NSDictionary+EmptyKeyMethods.h
//  QuadranWorkOrders
//
//  Created by Mark Menard on 5/1/12.
//  Copyright (c) 2012 Enable Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (EmptyKeyMethods)
- (NSString *)valueOrEmptyString:(NSString *)key;
- (NSData *)asJSON;

- (NSDictionary *)stripNulls;
@end
