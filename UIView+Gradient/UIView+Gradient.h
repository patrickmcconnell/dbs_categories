//
//  UIView+Gradient.h
//  Landlordia
//
//  Created by Patrick McConnell on 3/27/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//



@interface UIView (Gradient)

- (void)addGradientLayerFromColor:(UIColor *)fromColor 
                          toColor:(UIColor *)toColor;


@end
