//
//  UIView+Gradient.m
//  Landlordia
//
//  Created by Patrick McConnell on 3/27/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//

#import "UIView+Gradient.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (Gradient)

// adds a gradient to the view from top to bottom
// specified between two colors (fromColor and toColor)
- (void)addGradientLayerFromColor:(UIColor *)fromColor 
                          toColor:(UIColor *)toColor{
    //set these values to vary the gradient colors of the cell background
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[fromColor CGColor], (id)[toColor CGColor], nil];
    [self.layer insertSublayer:gradient atIndex:0];
}



@end
