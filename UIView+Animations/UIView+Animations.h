//
//  UIView+Animations.h
//  Landlordia
//
//  Created by Patrick McConnell on 5/11/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Animations)

- (void)wiggle;

@end
