//
//  UIView+Animations.m
//  Landlordia
//
//  Created by Patrick McConnell on 5/11/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIView+Animations.h"

@implementation UIView (Animations)

- (void)wiggle{
    NSLog(@"Wiggling View");
    NSString *keyPath = @"anchorPoint.x";    
    CAKeyframeAnimation *kfa = [CAKeyframeAnimation animationWithKeyPath:keyPath];
    [kfa setValues:[NSArray arrayWithObjects:
                    [NSNumber numberWithFloat:-.05],
                    [NSNumber numberWithFloat:.1],
                    [NSNumber numberWithFloat:-.05],
                    [NSNumber numberWithFloat:.1],
                    [NSNumber numberWithFloat:-.05],
                    nil]];    
    [kfa setDuration:.35];
    [kfa setAdditive:YES];
    [kfa setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.layer addAnimation:kfa forKey:nil];
}


@end
