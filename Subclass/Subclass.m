//
//  Subclass.m
//  Landlordia
//
//  Created by Patrick Mcconnell on 6/20/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//

#import "Subclass.h"

@implementation Subclass

void SubclassResponsibility(id classOrInstance, SEL _cmd)
{
    NSString *reason = [NSString stringWithFormat: @"Must subclass %s and override the method %s.",
                        object_getClassName(classOrInstance),
                        sel_getName(_cmd)];
    
    @throw [NSException exceptionWithName: @"Subclass"
                                   reason: reason
                                 userInfo: nil];
}


@end
