//
//  Subclass.h
//  Landlordia
//
//  Created by Patrick Mcconnell on 6/20/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subclass : NSObject


void SubclassResponsibility(id classOrInstance, SEL _cmd);

@end
