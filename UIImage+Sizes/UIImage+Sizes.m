//
//  UIImage+Sizes.m
//  Landlordia
//
//  Created by Patrick McConnell on 5/22/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//

#import "UIImage+Sizes.h"

@implementation UIImage (Sizes)

- (UIImage *)setSizeToWidth:(int) width andHeight:(int) height{
    if (self.size.width != width && self.size.height != height)
    {
        CGSize itemSize = CGSizeMake(width, height);
        UIGraphicsBeginImageContext(itemSize);
        CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
        [self drawInRect:imageRect];
        UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return resizedImage;
    } else {
        return self;
    }
    
}


@end
