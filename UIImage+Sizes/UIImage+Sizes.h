//
//  UIImage+Sizes.h
//  Landlordia
//
//  Created by Patrick McConnell on 5/22/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Sizes)

- (UIImage *)setSizeToWidth:(int) width andHeight:(int)height;

@end
