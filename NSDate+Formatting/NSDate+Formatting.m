//
//  NSDate+Formatting.m
//  Landlordia
//
//  Created by Patrick McConnell on 2/28/13.
//  Copyright (c) 2013 Dogboy Studios. All rights reserved.
//

#import "NSDate+Formatting.h"

@implementation NSDate (Formatting)

- (NSString *)shortFormatString
{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateStyle:NSDateFormatterShortStyle];
  
  return [dateFormatter stringFromDate:self];
}


- (NSString *)mediumFormatString
{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
  
  return [dateFormatter stringFromDate:self];
}


- (NSString *)longFormatString
{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateStyle:NSDateFormatterLongStyle];
  
  return [dateFormatter stringFromDate:self];
}

- (NSString *)yyyyMMddFormatString
{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd"];

//  NSLog(@"yyyy Date: %@", [dateFormatter stringFromDate:self]);
  
  return [dateFormatter stringFromDate:self];
}


@end
