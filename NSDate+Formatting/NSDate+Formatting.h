//
//  NSDate+Formatting.h
//  Landlordia
//
//  Created by Patrick McConnell on 2/28/13.
//  Copyright (c) 2013 Dogboy Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Formatting)

- (NSString *)shortFormatString;
- (NSString *)mediumFormatString;
- (NSString *)longFormatString;
- (NSString *)yyyyMMddFormatString;


@end
