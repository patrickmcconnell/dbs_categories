//
//  NSString+Formatting.h
//  Landlordia
//
//  Created by Patrick McConnell on 2/28/13.
//  Copyright (c) 2013 Dogboy Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Formatting)

+(NSString*)ordinalNumberFormat:(NSInteger)num;

- (NSString *)asCurrency;
- (NSString *)decimalString;
- (double)doubleFromCurrency;
- (NSDate *)yyyyMMddDate;
- (NSDate *)dateFromShortFormatDateString;
- (NSDate *)dateFromMediumFormatDateString;
- (NSDate *)dateFromLongFormatDateString;
- (int)intByStrippingLowerCaseCharacters;

@end
