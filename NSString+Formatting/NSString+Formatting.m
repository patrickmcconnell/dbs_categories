//
//  NSString+Formatting.m
//  Landlordia
//
//  Created by Patrick McConnell on 2/28/13.
//  Copyright (c) 2013 Dogboy Studios. All rights reserved.
//

#import "NSString+Formatting.h"

@implementation NSString (Formatting)


+(NSString*)ordinalNumberFormat:(NSInteger)num{
  NSString *ending;
  
  int ones = num % 10;
  int tens = floor(num / 10);
  tens = tens % 10;
  if(tens == 1){
    ending = @"th";
  }else {
    switch (ones) {
      case 1:
        ending = @"st";
        break;
      case 2:
        ending = @"nd";
        break;
      case 3:
        ending = @"rd";
        break;
      default:
        ending = @"th";
        break;
    }
  }
  return [NSString stringWithFormat:@"%d%@", num, ending];
}


- (NSString *)asCurrency
{
  // currency formatter
  NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];

  // set options.
  [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
  [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];

  // get formatted string
  return [currencyStyle stringFromNumber:@([self floatValue])];
}

- (double)doubleFromCurrency
{
  // currency formatter
  NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
  
  // set options.
  [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
  [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
  
  return [[currencyStyle numberFromString:self]doubleValue];
}


- (NSString *)decimalString
{
  NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
  
  // set options.
  [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
  [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
  [currencyStyle setLenient:YES];
  
  // get formatted string
  NSLog(@"Decimal String: %@", [NSString stringWithFormat:@"%0.2f",[[currencyStyle numberFromString:self] floatValue] ]);
  return [NSString stringWithFormat:@"%0.2f",[[currencyStyle numberFromString:self] floatValue] ];
}

- (NSDate *)yyyyMMddDate
{
  if ([self isEqualToString:@""]) {
    return nil;
  }
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd"];
  
  NSString *dateStr = self;
  
  NSRange range = [self rangeOfString:@"T"];
  if (range.location != NSNotFound ) {
     dateStr = [self substringToIndex:range.location];
  }
  
  NSLog(@"self: %@ yyyy: %@", dateStr, [dateFormatter dateFromString:dateStr]);
  
  return [dateFormatter dateFromString:dateStr];
}

- (NSDate *)dateFromShortFormatDateString {
  if ([self isEqualToString:@""]) {
    return nil;
  }
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateStyle:NSDateFormatterShortStyle];
  
  return [dateFormatter dateFromString:self];
}


- (NSDate *)dateFromMediumFormatDateString {
  if ([self isEqualToString:@""]) {
    return nil;
  }
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
  
  return [dateFormatter dateFromString:self];
}


- (NSDate *)dateFromLongFormatDateString {
  if ([self isEqualToString:@""]) {
    return nil;
  }
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateStyle:NSDateFormatterLongStyle];
  
  return [dateFormatter dateFromString:self];
}


- (int)intByStrippingLowerCaseCharacters
{
  NSCharacterSet *charactersToRemove =
  [[NSCharacterSet alphanumericCharacterSet ] invertedSet];

  NSString *trimmedReplacement =
  [[self componentsSeparatedByCharactersInSet:charactersToRemove]
   componentsJoinedByString:@""];
  
  return [trimmedReplacement integerValue];
}
@end
