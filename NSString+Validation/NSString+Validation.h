//
//  NSString+Validation.h
//  Landlordia
//
//  Created by Patrick McConnell on 4/3/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//



@interface NSString (Validation)

-(BOOL)isValidEmail;
- (BOOL)isEmpty;

@end
