//
//  NSString+Validation.m
//  Landlordia
//
//  Created by Patrick McConnell on 4/3/12.
//  Copyright (c) 2012 Dogboy Studios. All rights reserved.
//

#import "NSString+Validation.h"

@implementation NSString (Validation)

-(BOOL)isValidEmail{
  if ([self isEqualToString:@""]) return NO;
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isEmpty {
  if([self length] == 0) { //string is empty or nil
    return YES;
  }
  
  if(![[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
    //string is all whitespace
    return YES;
  }
  
  return NO;
}


@end
